package com.learning.newspring;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class BadException extends RuntimeException{

    public BadException(final String message) {
        super(message);
    }
}
