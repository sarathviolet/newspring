package com.learning.newspring.controllers;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.learning.newspring.pojo.Student;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping(value = "/api")
public class MainController {
    private final ObjectMapper mapper = new ObjectMapper();

    @PostMapping(value = "/studentread")
    public String getValue (@RequestBody String student) throws IOException {
        Student studentX = mapper.readValue(student, Student.class);
        return studentX.toString();
    }

    @GetMapping(value =  "/studentwrite")
    public String setValue (Student student) throws JsonGenerationException, JsonMappingException, IOException{
        String json = mapper.writeValueAsString(student);
        return  json;
    }
/*
    @RequestMapping(value = "/test/postmethod/sayhello", method = RequestMethod.POST, consumes = "application/json")
    public String getData(@RequestBody Student student) {
        return "Student Name is :" + student.getName() + " Age : " + student.getAge() + " Gender is : " + student.getGender();
    }

    @GetMapping("/employees/{id}/{name}")
    public String getEmployeesById(@PathVariable String id, @PathVariable String name) {
        return "ID: " + id + ", NAME: " + name;
    }
*/
}
