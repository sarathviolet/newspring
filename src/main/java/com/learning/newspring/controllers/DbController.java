package com.learning.newspring.controllers;


import com.learning.newspring.dto.StudentAdderDto;
import com.learning.newspring.dto.StudentResponderDto;
import com.learning.newspring.services.DbService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class DbController {

    DbService dbService;

    public DbController(DbService dbService) {
        this.dbService = dbService;
    }

    @PostMapping("/add_student")
    public StudentResponderDto addStudent (StudentAdderDto studentAdderDto){
        StudentResponderDto studentResponderDto = dbService.addStudent(studentAdderDto);
        return studentResponderDto;
    }

    @PostMapping("/find_student")
    public StudentResponderDto viewStudents (@RequestBody  Long id){
        StudentResponderDto studentResponderDto = dbService.findStudent(id);
        return studentResponderDto;
    }

 }
