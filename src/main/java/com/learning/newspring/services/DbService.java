package com.learning.newspring.services;

import com.learning.newspring.BadException;
import com.learning.newspring.Repository.StudAddressRepository;
import com.learning.newspring.Repository.StudentRepository;
import com.learning.newspring.dto.StudentAdderDto;
import com.learning.newspring.dto.StudentResponderDto;
import com.learning.newspring.pojo.StudAddress;
import com.learning.newspring.pojo.Student;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DbService {
    public StudentRepository studentRepository;
    public StudAddressRepository studAddressRepository;

    public DbService(StudentRepository studentRepository1, StudAddressRepository studAddressRepository1) {
        this.studentRepository = studentRepository1;
        this.studAddressRepository = studAddressRepository1;
    }

    public StudentResponderDto addStudent(StudentAdderDto studentAdderDto) {
        Student student = new Student();
        StudAddress studAddress = new StudAddress();

        student.setName(studentAdderDto.getName());
        student.setAge(studentAdderDto.getAge());
        student.setGender(studentAdderDto.getGender());
        Student reStudent = studentRepository.save(student);

        studAddress.setStudId(reStudent.getId());
        studAddress.setCity(studentAdderDto.getCity());
        studAddress.setStreet(studentAdderDto.getStreet());
        studAddress.setCountry(studentAdderDto.getCountry());
        studAddress.setZipcode(studentAdderDto.getZipcode());
        StudAddress reStudAddress = studAddressRepository.save(studAddress);

        StudentResponderDto studentResponderDto = new StudentResponderDto();
        studentResponderDto.setId(reStudent.getId());
        studentResponderDto.setAdId(reStudAddress.getAdId());
        studentResponderDto.setStudId(reStudAddress.getStudId());
        studentResponderDto.setName(reStudent.getName());
        studentResponderDto.setGender(reStudent.getGender());
        studentResponderDto.setAge(reStudent.getAge());
        studentResponderDto.setCity(reStudAddress.getCity());
        studentResponderDto.setStreet(reStudAddress.getStreet());
        studentResponderDto.setCountry(reStudAddress.getCountry());
        studentResponderDto.setZipcode(reStudAddress.getZipcode());

        return studentResponderDto;
    }


    public StudentResponderDto findStudent(Long id) {

        Optional<Student> student = studentRepository.findById(id);
        StudentResponderDto studentResponderDto = new StudentResponderDto();
        if(student.isPresent() && student != null){

            StudAddress studAddress = studAddressRepository.findByStudId(student.get().getId());
            studentResponderDto.setId(student.get().getId());
            studentResponderDto.setAdId(studAddress.getAdId());
            studentResponderDto.setStudId(studAddress.getStudId());
            studentResponderDto.setName(student.get().getName());
            studentResponderDto.setGender(student.get().getGender());
            studentResponderDto.setAge(student.get().getAge());
            studentResponderDto.setCity(studAddress.getCity());
            studentResponderDto.setStreet(studAddress.getStreet());
            studentResponderDto.setCountry(studAddress.getCountry());
            studentResponderDto.setZipcode(studAddress.getZipcode());
        }
        else{
            throw new BadException("name not found");

        }
        return studentResponderDto;
    }
}
