package com.learning.newspring.Repository;

import com.learning.newspring.pojo.StudAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudAddressRepository extends JpaRepository<StudAddress, Long> {
    StudAddress findByStudId(Long studId);
}
