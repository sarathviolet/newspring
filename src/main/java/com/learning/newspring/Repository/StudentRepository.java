package com.learning.newspring.Repository;

import com.learning.newspring.pojo.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    Optional<Student> findById(Long id);

//    @Query("SELECT * FROM public.student\n" +
//            "ORDER BY id ASC ")
    //Page<Student> findBySearch(String search, Pageable pageable);
    //Student findByName(String name);

    //Optional<Student> findByName(String name);
   //Page<Student> findAll(Pageable pageable);
}
