package com.learning.newspring.pojo;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class StudAddress {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long adId;

  //  @ManyToOne
    @JoinColumn(name = "stud_id")
    private Long studId;
    private String street;
    private String city;
    private int zipcode;
    private String country;
}
