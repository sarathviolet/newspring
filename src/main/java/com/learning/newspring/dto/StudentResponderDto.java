package com.learning.newspring.dto;

import lombok.Data;

@Data
public class StudentResponderDto {
    private Long id;

    private String name;
    private String gender;
    private int age;
    private Long adId;
    private Long studId;
    private String street;
    private String city;
    private int zipcode;
    private String country;
}
