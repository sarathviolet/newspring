package com.learning.newspring.dto;

import lombok.Data;

@Data
public class StudentAdderDto {

    private String name;
    private String gender;
    private int age;

    private String street;
    private String city;
    private int zipcode;
    private String country;


}
