CREATE TABLE address (
	ad_id INT PRIMARY KEY,
	stud_id INT NOT NULL,
	    FOREIGN KEY (stud_id)
          REFERENCES student (id),
	street VARCHAR ( 50 ) UNIQUE NOT NULL,
	city VARCHAR ( 50 ) NOT NULL,
	zipcode INT NOT NULL,
	country VARCHAR ( 50 ) UNIQUE NOT NULL
);